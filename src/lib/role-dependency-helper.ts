import fs from 'fs'
import os from 'os'
import path from 'path'
import { promisify } from 'util'

import _glob from 'glob'
import { readFile, writeFile } from 'jsonfile'
import yaml from 'yaml'

const glob = promisify(_glob)

import { extractData } from './helper'
import { Logger } from './log'

const ANSIBLE_CFG_FILENAME = 'ansible.cfg'

export type TDependencyMap = {
  readonly [key: string]: readonly string[]
}

/**
 * Find all requirements.yml files of all ansible roles from the ansible roles directory
 *
 * @param {string} roles Roles directory
 * @returns {Promise<string[]>} Promise that resolves to array of requirements.yml file paths relative to current working directory
 */
export async function findRequirementsYAMLs(roles: string): Promise<readonly string[]> {
  // look for meta/main.yml from the given role path
  const files = await glob(`${roles}/*/*/requirements.yml`)

  return files
}

/**
 * Get dependency roles using the requirements.yml file
 *
 * @param {string} requirementsYamlFilePath requirements.yml file path of the ansible role
 * @returns {string[]} Dependency roles found in requirements.yml file
 */
export function getDependencyRoles(requirementsYamlFilePath: string): readonly string[] {
  const yml = yaml.parse(fs.readFileSync(requirementsYamlFilePath, { encoding: 'utf8' }))
  if (!yml.roles || yml.roles.length === 0) {
    return []
  }

  return yml.roles.reduce((previous, role) => {
    if (!role || !role.name) {
      return previous
    }

    return [...previous, role.name]
  }, [])
}

/**
 * Inject individual role dependency to package.json of each role
 *
 * @param {string} requirementsYamlFilePath Absolute file path to requirements.yml
 * @param {string[][]} chart Generated dependency chart
 */
export async function injectRoleDependency(
  requirementsYamlFilePath: string,
  chart: readonly (readonly string[])[]
): Promise<void> {
  // find the package.json
  const packageFilePath = path.join(path.dirname(requirementsYamlFilePath), 'package.json')
  if (!fs.existsSync(packageFilePath)) {
    const message = `${packageFilePath} does not exists`
    Logger.error(message)
    throw new Error(message)
  }

  // parse package.json
  const packageJson = await readFile(packageFilePath)
  if (!packageJson.blueprint) {
    // eslint-disable-next-line functional/immutable-data
    packageJson.blueprint = {}
  }

  // eslint-disable-next-line functional/immutable-data
  packageJson.blueprint.requirements = chart

  await writeFile(packageFilePath, packageJson, { spaces: 2 })
}

/**
 * Inject all role dependencies
 *
 * @param {TDependencyMap} roleDependencyMap Role dependencies Map found from the requirements.yml
 * @param {Map<string, string[]>} dependencyChartMap A Map of generated dependencies charts
 */
export async function injectRoleDependencies(
  roleDependencyMap: TDependencyMap,
  dependencyChartMap: ReadonlyMap<string, readonly string[]>
): Promise<void> {
  const injectPromises = Object.keys(roleDependencyMap).map(async (file): Promise<void> => {
    const chart = roleDependencyMap[file].map((dep): readonly string[] => {
      const depChart = dependencyChartMap.get(dep)
      if (depChart) {
        return depChart
      }

      return []
    })

    if (!chart) {
      return
    }

    await injectRoleDependency(file, chart)
  })

  await Promise.all(injectPromises)
}

/**
 * Find absolute file paths to query role dependencies
 *
 * This function depends on current working directory. It utilizes the ansible.cfg
 * file located in current working directory.
 *
 * @returns {string[]} Array of absolute file paths
 */
export function getRolePaths(): readonly string[] {
  const ansibleCfgFilePath = path.join(process.cwd(), ANSIBLE_CFG_FILENAME)

  if (!fs.existsSync(ansibleCfgFilePath)) {
    throw new Error(`Ansible config file cannot be found at ${ansibleCfgFilePath}`)
  }

  const config = fs.readFileSync(ansibleCfgFilePath, { encoding: 'utf8' })
  const rolesPaths = config.match(/roles_path\s*=\s*(.*)/)

  if (rolesPaths === null || !rolesPaths[1]) {
    throw new Error(`Unable to find roles path from ${ansibleCfgFilePath}`)
  }

  // parse roles_path
  const paths = rolesPaths[1].split(':')

  const absPaths = paths.map((rolesPath) => {
    if (path.isAbsolute(rolesPath)) {
      return rolesPath
    }

    if (/^roles\//.test(rolesPath)) {
      return path.join(process.cwd(), rolesPath)
    }

    if (rolesPath.startsWith('~')) {
      return rolesPath.replace('~', os.homedir())
    }

    throw new Error(`Unknown path found. ${rolesPath}`)
  })

  return absPaths
}

/**
 * Get roles directories inside the base path
 *
 * @param {string} basePath Base path to find role directories
 * @returns {string[]} A promise that resolves to array of absolute file paths
 */
// eslint-disable-next-line require-await
export const getRolesInPath = async (basePath: string): Promise<readonly string[]> =>
  glob('*/', {
    absolute: true,
    cwd: basePath
  })

/**
 * Get unique dependencies from the roleDependencyMap
 *
 * @param {TDependencyMap} roleDependencyMap Object of file to dependencies found on requirements.yml file
 * @returns {Set<string>} A Set of unique dependencies
 */
export function getUniqueDependencies(roleDependencyMap: TDependencyMap): ReadonlySet<string> {
  const dependencies = new Set<string>()
  Object.keys(roleDependencyMap).map((role): void => {
    roleDependencyMap[role].map((dependency): void => {
      if (!dependencies.has(dependency)) {
        dependencies.add(dependency)
      }
    })
  })

  return dependencies
}

/**
 * Build a Object of requirements.yml to dependencies
 *
 * @param {string[]} files Array of requirements.yml files
 * @returns {TDependencyMap} Object of file to dependencies found on requirements.yml file
 */
export const buildRoleDependencyMap = (files: readonly string[]): TDependencyMap =>
  files.reduce((previous, file) => {
    // get dependency roles for each role
    const deps = getDependencyRoles(file)
    // no dependency?
    if (deps.length === 0) {
      Logger.debug(`No dependencies for ${file}`)

      return previous
    }

    return {
      ...previous,
      [file]: deps
    }
  }, {})

/**
 * Generate a Map of dependency to system file path
 *
 * This function uses the ansible.cfg file to find the locations to query from
 *
 * @param {string} namespace Ansible Galaxy namespace
 * @param {Set<string>} dependenciesSet Dependencies to be found
 * @returns {Promise<Map<string, string>>} Map of dependencies to system file path
 */
export async function findDependencyRoleLocations(
  namespace: string,
  dependenciesSet: ReadonlySet<string>
): Promise<ReadonlyMap<string, string>> {
  const dependencyLocations = new Map<string, string>()

  const rolesMapPromises = getRolePaths().map(async (rolePath): Promise<void> => {
    const paths = await getRolesInPath(rolePath)
    // no roles inside path?
    if (paths.length === 0) {
      return
    }

    // are there any valid dependency role among paths?
    paths.map((roleDirectory) => {
      // eslint-disable-next-line unicorn/prefer-string-slice
      const directory = roleDirectory.substring(roleDirectory.length, roleDirectory.lastIndexOf('/') + 1)
      const key = `${namespace}.${directory}`

      // don't overwrite roles to keep the priority as in role paths order
      if (dependenciesSet.has(key) && !dependencyLocations.has(key)) {
        dependencyLocations.set(key, roleDirectory)
      }
    })
  })

  await Promise.all(rolesMapPromises)

  return dependencyLocations
}

/**
 * Find the missing dependencies that could not found from the system
 *
 * @param {Set<string>} dependenciesSet A Set of all dependencies found
 * @param {Map<string, string>} roleLocationsMap A Map of role to system location
 * @returns {string[]} Returns array of dependencies that were not able to find in the roleLocationsMap
 */
export function getMissingDependencies(
  dependenciesSet: ReadonlySet<string>,
  roleLocationsMap: ReadonlyMap<string, string>
): readonly string[] {
  // eslint-disable-next-line functional/prefer-readonly-type
  const missing: string[] = []
  ;[...dependenciesSet].map((dependency) => {
    if (!roleLocationsMap.has(dependency)) {
      // eslint-disable-next-line functional/immutable-data
      missing.push(dependency)
    }
  })

  return missing
}

/**
 * This function returns a dependency chart as a Map
 *
 * @param {Map<string, string>} roleLocationsMap Map of ansible role to system path
 * @returns {Promise<Map<string, string[]>>} Returns Map of role to dependency chart
 */
export async function getDependencyCart(
  roleLocationsMap: ReadonlyMap<string, string>
): Promise<ReadonlyMap<string, readonly string[]>> {
  const map = new Map<string, readonly string[]>()
  await Promise.all(
    [...roleLocationsMap].map(async ([dependency, location]) => {
      const chart = await extractData(path.join(location, 'meta/main.yml'))
      map.set(dependency, chart)
    })
  )

  return map
}

import { exec as EXEC } from 'child_process'
import path from 'path'
import { promisify } from 'util'

import jsonfile from 'jsonfile'

import { Logger } from './log'

const exec = promisify(EXEC)

const platforms = {}
let alias = {}

/**
 * Get platform version code name for the given version number
 *
 * @param {string} platform Platform name
 * @param {string} version Version number
 * @returns {string | undefined} Returns the code name. If there's no code name found returns undefined
 */
export async function getPlatformVersionName(platform: string, version: string): Promise<string | undefined> {
  const platformKey = platform.toLowerCase()
  if (platforms[platformKey]) {
    return platforms[platformKey][version]
  }

  // eslint-disable-next-line unicorn/prefer-module
  const filePath = path.resolve(__dirname, `../../../static/${platformKey}-versions.json`)
  try {
    const platformData = await jsonfile.readFile(filePath)
    // eslint-disable-next-line functional/immutable-data
    platforms[platformKey] = platformData

    if (platforms[platformKey]) {
      return platforms[platformKey][version]
    }

    return undefined
  } catch {
    return undefined
  }
}

/**
 * Get Platform alias for the given name. If not found it wil return the given name
 *
 * @param {string} name Platform name
 * @returns {Promise<string>} Alias
 */
export async function getPlatformAlias(name: string): Promise<string> {
  if (alias[name] !== undefined) {
    return alias[name]
  }

  // eslint-disable-next-line unicorn/prefer-module
  const osMappingFile = path.resolve(__dirname, '../../../static/os.json')
  try {
    const osMapping = await jsonfile.readFile(osMappingFile)
    alias = osMapping
    if (osMapping[name]) {
      return osMapping[name]
    }

    // eslint-disable-next-line functional/immutable-data
    alias[name] = name

    return name
  } catch {
    return name
  }
}

/**
 * Format validation errors to user friendly error messages
 *
 * @param {any} errors Errors object
 * @returns {string} Error messages as formatted string
 */
export function formatValidationErrors(errors): string {
  const messages = Object.keys(errors).map((key) => {
    const error = errors[key]

    return Object.values(error.constraints).join(',')
  })

  return messages.join('\n')
}

/**
 * Generate roles using ansible-galaxy command
 *
 * This function assumes ansible-galaxy installed on current system
 *
 * @param {string} namespace Role namespace
 * @param {string[]} missing Missing ansible dependencies with namespaces
 */
export async function generateRoles(namespace: string, missing: readonly string[]): Promise<void> {
  const promises = missing.map(async (dependency) => {
    try {
      const { stderr } = await exec(`ansible-galaxy info ${namespace}.${dependency}`)
      if (stderr) {
        throw new Error(stderr)
      }
    } catch (error) {
      Logger.error(`Failed to generate role for ${namespace}.${dependency}`)
      Logger.error(error)
    }
  })
  await Promise.all(promises)
}

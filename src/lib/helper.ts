import fs from 'fs'
import readline from 'readline'

/**
 * Find the role name from the file path of the main.yml
 *
 * @param {string} filePath File path to the main.yml file
 * @returns {string} Role name
 */
export function getRoleName(filePath: string): string {
  const matches = filePath.match(/\/([\w-]+)\/meta\/main\.yml$/)
  if (matches === null) {
    return ''
  }

  return matches[1]
}

/**
 * Extract data from the given yml file
 *
 * @param {string} ymlFilePath file path
 * @returns {string} Extracted data as array
 */
export async function extractData(ymlFilePath: string): Promise<readonly string[]> {
  const rl = readline.createInterface({
    input: fs.createReadStream(ymlFilePath),
    output: process.stdout,
    terminal: false
  })

  const promise = await new Promise((resolve, reject) => {
    let description: string = ''
    let roleName: string = ''
    rl.on('line', (line) => {
      const matches = line.match(/^\s+(description|role_name):\s*(.*)$/)
      if (matches) {
        if (matches[1] === 'role_name') {
          roleName = matches[2]
        } else if (matches[1] === 'description') {
          description = matches[2]
        }
      }
      // has found both
      if (roleName !== '' && description !== '') {
        rl.close()
      }
    })

    rl.on('close', () => {
      if (roleName === '') {
        // get the roleName from the file path
        roleName = getRoleName(ymlFilePath)
      }
      resolve([roleName, description])
    })

    rl.on('SIGTSTP', () => {
      reject(new Error('SIGTSTP received'))
    })
  })

  return promise as Promise<readonly string[]>
}

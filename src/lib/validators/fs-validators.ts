import fs from 'fs'
import path from 'path'

import { registerDecorator, ValidationOptions } from 'class-validator'

/**
 * Validate given property value is a valid directory
 *
 * @param {any} property validation property
 * @param {ValidationOptions} validationOptions Validation options
 * @returns {Function} Returns validation function
 */
export const IsDirectory =
  (property: string, validationOptions?: ValidationOptions): Function =>
  (object: object, propertyName: string): void => {
    registerDecorator({
      constraints: [property],
      name: 'IsDirectory',
      options: validationOptions,
      propertyName: propertyName,
      target: object.constructor,
      validator: {
        async validate(value: unknown): Promise<boolean> {
          if (!value) {
            return false
          }

          try {
            return await !fs.lstatSync(value as string).isFile()
          } catch {
            return false
          }
        }
      }
    })
  }

/**
 * Validate that given property value is a valid file
 *
 * @param {any} property validation property
 * @param {ValidationOptions} validationOptions Validation options
 * @returns {Function} Returns validation function
 */
export const IsFileExists =
  (property: unknown, validationOptions?: ValidationOptions): Function =>
  (object: object, propertyName: string): void => {
    registerDecorator({
      constraints: [property],
      name: 'IsFileExists',
      options: validationOptions,
      propertyName,
      target: object.constructor,
      validator: {
        validate: (value: unknown): boolean => {
          if (!value) {
            return false
          }

          return !fs.existsSync(value as string)
        }
      }
    })
  }

/**
 * Validate the given property value contains in a writable directory
 *
 * @param {any} property validation property
 * @param {ValidationOptions} validationOptions Validation options
 * @returns {Function} Returns validation function
 */
export const IsWritable =
  (property: unknown, validationOptions?: ValidationOptions): Function =>
  (object: object, propertyName: string): void => {
    registerDecorator({
      constraints: [property],
      name: 'IsWritable',
      options: validationOptions,
      propertyName,
      target: object.constructor,
      validator: {
        validate: (value: unknown): boolean => {
          if (!value) {
            return false
          }

          const baseDirectory = path.dirname(value as string)
          try {
            fs.accessSync(baseDirectory, fs.constants.W_OK)

            return true
          } catch {
            return false
          }
        }
      }
    })
  }

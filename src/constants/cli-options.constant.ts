const COMMAND = 'ansibler'
export const CLI_OPTIONS = {
  append: 'Version 0.0.1',
  options: [
    {
      alias: 'h',
      description: 'Displays help',
      option: 'help',
      type: 'Boolean'
    },
    {
      alias: 'r',
      description: 'Path to Ansible roles directory',
      example: `${COMMAND} --roles  /path/to/roles/directory`,
      option: 'roles',
      type: 'String'
    },
    {
      alias: 'o',
      default: `./${COMMAND}-out.json`,
      description: 'Relative path to the output file',
      example: `${COMMAND} --output ./${COMMAND}-out.json`,
      option: 'output',
      type: 'String'
    },
    {
      description: 'GitLab base URL',
      example: `${COMMAND} --base-url https://gitlab.com/megabyte-labs/ansible-roles`,
      option: 'base-url',
      type: 'String'
    },
    {
      description: 'GitLab username',
      example: `${COMMAND} --username myUsername`,
      option: 'username',
      type: 'String'
    },
    {
      description: 'Populate description',
      example: `${COMMAND} --populate-descriptions`,
      option: 'populate-descriptions',
      type: 'Boolean'
    },
    {
      default: 'ansible-molecule.json',
      description: 'The filename output by ansible-molecule-json tool',
      example: `${COMMAND} --molecule-filename ansible-molecule.json`,
      option: 'molecule-filename',
      type: 'String'
    },
    {
      description: 'Populate compatibility platforms',
      example: `${COMMAND} --populate-platforms`,
      option: 'populate-platforms',
      type: 'Boolean'
    },
    {
      description: 'Populate role dependencies',
      example: `${COMMAND} --populate-role-dependencies`,
      option: 'populate-role-dependencies',
      type: 'Boolean'
    }
  ],
  prepend: `Usage: ${COMMAND} [options]`
}

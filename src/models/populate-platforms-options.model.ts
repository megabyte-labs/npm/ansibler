import { IsString } from 'class-validator'

import { IsDirectory } from '../lib/validators/fs-validators'

export class PopulatePlatformsOptions {
  @IsDirectory('roles', {
    message: 'roles must be a valid directory'
  })
  public readonly roles: string

  @IsString()
  public readonly moleculeFilename: string
  constructor(options: PopulatePlatformsOptions) {
    this.roles = options.roles
    this.moleculeFilename = options.moleculeFilename
  }
}
